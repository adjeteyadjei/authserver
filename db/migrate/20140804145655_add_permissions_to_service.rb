class AddPermissionsToService < ActiveRecord::Migration
  def change
    add_column :services, :permissions, :string, array: true, default: '{}'
  end
end
