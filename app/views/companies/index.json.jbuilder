json.array!(@companies) do |company|
  json.extract! company, :id, :name, :postal_address, :location, :telephone_number
  json.url company_url(company, format: :json)
end
