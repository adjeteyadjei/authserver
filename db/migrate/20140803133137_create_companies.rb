class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :postal_address
      t.string :location
      t.string :telephone_number

      t.timestamps
    end
  end
end
